# Gatsby Plugin .well-known

Add files to the .well-known directory during build.  
The files can use variables.

## Getting started

1. Install the package with npm or yarn:  
`npm install gatsby-plugin-well-known-pages`  
`yarn add gatsby-plugin-well-known-pages`
2. Add plugin configuration to gatsby-config.js:

```
module.exports = {
    "plugins": ['gatsby-plugin-well-known-pages'],
}
```
3. Create a directory `.well-known` in your project root and add your files here
4. (Optional) If you need templating for the files change the gatsby-config.js like this:  
```
{
    resolve: 'gatsby-plugin-well-known-pages',
    options: {
        pages: [
            {
                fileName: 'security.txt',
                variables: {
                    __EXPIRES__: '2022-12-31T22:59:00.000Z',
                },
            },
        ],
    },
},
```
you can then use the variable inside of your file like this: `The time this expires is __EXPIRES__`  
There is no rule on how the variable should look. The plugin will just look for the string you give it and replace it with the value.
